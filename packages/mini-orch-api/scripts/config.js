/* eslint-disable sort-keys */
const fs = require('fs')
const path = require('path')
const { concat, filter, reduce } = require('lodash')

const mainFolder = 'mini-orch'

const { join, resolve } = path
const res = (...p) => resolve(__dirname, '..', ...p)
const packagesRes = (...p) => res('..', ...p)
const rootRes = (...p) => res('..', '..', ...p)
const mainRes = (...p) => res(mainFolder, ...p)

const env = process.env.NODE_ENV

console.info(`NODE_ENV: ${env}`)
const envPath = env === 'production' ? 'prod' : 'dev'

const globals = {
  'process.env.NODE_ENV': JSON.stringify(env),
  __ENV__: JSON.stringify(env),
  __DEV__: env === 'development' ? 'true' : 'false',
}

const folders = {
  build: res('dist'),
  main: mainRes(),
  api: mainRes('api'),
  worker: mainRes('worker'),
  temp: res('node_modules/.cache/build'),
  webpack: res('webpack'),
}

const localFoldersRegex = /\.bin|\.cache|\.yarn-integrity/
const getExternals = () =>
  reduce(
    filter(
      concat(fs.readdirSync(res('node_modules')), fs.readdirSync(rootRes('node_modules'))),
      mod => !localFoldersRegex.test(mod)
    ),
    (modules, mod) => {
      modules[mod] = `commonjs ${mod}`
      return modules
    },
    {}
  )

exports.mainRes = mainRes

exports.res = res

exports.env = env

const devTools = '@dev-tools'
const stitData = '@stit-data'

exports.build = {
  entry: {
    api: join(folders.api, 'index.js'),
    worker: join(folders.worker, 'index.js'),
  },
  externals: getExternals,
  globals: globals,
  output: folders.build,
  babel: rootRes('.babelrc'),
  resolve: {
    alias: {
      [stitData]: packagesRes(stitData),
      [devTools]: packagesRes(devTools),
      src: mainRes('src'),
    },
    extensions: ['.js', '.jsx', '.json'],
    modules: [res('node_modules'), rootRes('node_modules'), 'lib'],
  },
}

exports.webpack = {
  config: join(folders.webpack, `webpack.${envPath}.js`),
  stats: {
    assets: env === 'production',
    builtAt: true,
    children: false,
    chunks: false,
    colors: true,
    context: mainRes(),
    entrypoints: env === 'production',
    env: true,
    hash: env === 'production',
    modules: false,
    timings: true,
    version: false,
  },
}
/* eslint-enable sort-keys */
