const webpack = require('webpack')
const rimraf = require('rimraf')
const { replace } = require('lodash')

const config = require('./config')
// eslint-disable-next-line security/detect-non-literal-require
const webpackConfig = require(config.webpack.config)

rimraf.sync(config.build.output)

const compiler = webpack(webpackConfig)
console.info('webpacking...')
compiler.watch(
  {
    ignored: /node_modules|dist|logs/,
  },
  (err, stats) => {
    if (err) console.error(err)
    else {
      const result = stats.toString(config.webpack.stats)
      if (config.env !== 'development') console.info(result)
      else console.info(replace(result, '\n', ' | '))
    }
  }
)
