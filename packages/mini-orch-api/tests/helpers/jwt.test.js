import { omit } from 'lodash'

describe('jwt helper ', () => {
  beforeEach(() => {
    jest.resetModules()
  })

  it('should verify a token equals to claim', async () => {
    const { issue, verify } = require('src/helpers/jwt')

    const claims = { username: 'fake user' }
    const token = await issue(claims)

    const verified = await verify(token)

    expect(verified.iat).toBeDefined()

    expect(omit(verified, 'iat')).toEqual({
      ...claims,
      iss: 'mini-orch-test.stitdata.com',
    })
  })
})
