import createError from 'http-errors'
import { set } from 'lodash'

export const mockHttpError = (...args) => createError(...args)
export const mockHttpErrorFromJSON = (status, error) =>
  createError(status, JSON.stringify(error, null, 2))

export const mockContext = ({ state, body, request, query }) => {
  const ctx = {}
  set(ctx, 'state', state || {})
  set(ctx, 'body', body || {})
  set(ctx, 'request', request || {})
  set(ctx, 'query', query || {})
  ctx.throw = jest.fn().mockImplementation((...args) => {
    throw createError(...args)
  })

  return ctx
}
