import mongoose from 'mongoose'

import config from '../config'

describe('mongodb connect', () => {
  it('should connect to a valid uri', async () => {
    const mongodb = require('src/datastores/mongodb')

    const { datastores } = config
    const createConnectionSpy = jest.spyOn(mongoose, 'createConnection')

    const instance = await mongodb.connect(datastores.documentstore)

    await instance.createCollection('testCollection')

    expect(instance).toBeDefined()
    expect(createConnectionSpy).toHaveBeenCalledTimes(1)
    expect(createConnectionSpy).toHaveBeenCalledWith(datastores.documentstore, {
      useCreateIndex: true,
      useNewUrlParser: true,
    })

    expect.assertions(3)

    instance.close()
    createConnectionSpy.mockReset()
    createConnectionSpy.mockRestore()
  })
})
