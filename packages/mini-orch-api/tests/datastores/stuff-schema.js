import mongoose from 'mongoose'

import mongooseVersions from 'src/datastores/mongodb/plugins/mongoose-versions'

// eslint-disable-next-line max-params
export const stuffSchema = (modelName, store, versionStore, versionCollection) => {
  const Schema = mongoose.Schema

  const schema = new Schema({
    name: { type: String },
  })

  if (versionStore) {
    schema.plugin(mongooseVersions, {
      name: versionCollection || modelName,
      store: versionStore,
    })
  }

  store.model(modelName, schema)
}

// eslint-disable-next-line max-params
export const otherSchema = (modelName, store, versionStore, versionCollection) => {
  const Schema = mongoose.Schema

  const schema = new Schema({
    name: { type: String },
  })

  if (versionStore) {
    schema.plugin(mongooseVersions, {
      name: versionCollection || modelName,
      removeVersions: true,
      store: versionStore,
    })
  }

  store.model(modelName, schema)
}
