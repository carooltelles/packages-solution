import { otherSchema, stuffSchema } from './stuff-schema'

import config from '../config'

describe('mongoose-versions plugin ', () => {
  let Stuff
  let Other
  let documentStore
  let versionStore

  beforeAll(async () => {
    const mongodb = require('src/datastores/mongodb')
    const { datastores } = config

    const stores = await Promise.all([
      mongodb.connect(datastores.documentstore),
      mongodb.connect(datastores.versionstore),
    ])

    documentStore = stores[0]
    versionStore = stores[1]

    stuffSchema('Stuff', documentStore, versionStore)
    otherSchema('Other', documentStore, versionStore)

    Stuff = documentStore.model('Stuff')
    Other = documentStore.model('Other')
  })

  afterAll(async () => {
    await Promise.all([documentStore.close(), versionStore.close()])
  })

  it('should not save a version the first time a document is saved', async () => {
    const stuff = new Stuff({ name: 'stuff-name' })

    await stuff.save()

    expect(stuff._id).toBeDefined()

    expect.assertions(1)
  })

  it('should store a document version when it is updated', async () => {
    const stuff = new Stuff({ name: 'new-stuff-name' })

    await stuff.save()

    expect(stuff._id).toBeDefined()

    stuff.name = 'new name!'
    await stuff.save()

    stuff.name = 'other name!'
    await stuff.save()

    // eslint-disable-next-line lodash/prefer-lodash-method
    const versions = await Stuff.Version.find({ refId: stuff._id })

    const version = versions[1]

    expect(version.refId).toEqual(stuff._id)
    expect(version.refVersion).toBe(stuff.__v - 1)

    expect(versions).toHaveLength(2)
    expect.assertions(4)
  })

  it('should not remove the versions of a document when removed', async () => {
    const stuff = new Stuff({ name: 'to-remove-stuff-name' })
    await stuff.save()

    expect(stuff._id).toBeDefined()

    stuff.name = 'new name!'
    await stuff.save()

    stuff.name = 'other name!'
    await stuff.save()

    await Stuff.remove({ _id: stuff._id })

    // eslint-disable-next-line lodash/prefer-lodash-method
    const versions = await Stuff.Version.find({ refId: stuff._id })

    expect(versions).toHaveLength(2)
    expect.assertions(2)
  })

  it('should remove the versions of a document when removed', async () => {
    const other = new Other({ name: 'to-remove-stuff-name' })

    await other.save()

    expect(other._id).toBeDefined()

    other.name = 'new name!'
    await other.save()

    other.name = 'other name!'
    await other.save()

    await other.remove({ _id: other._id })

    // eslint-disable-next-line lodash/prefer-lodash-method
    const versions = await Stuff.Version.find({ refId: other._id })

    expect(versions).toHaveLength(0)
    expect.assertions(2)
  })
})
