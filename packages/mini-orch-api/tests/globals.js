const mockConfig = require('./config')
const log = require('debug')('test:setup')

log('Setting globals ... ')
jest.mock('../mini-orch/src/config', () => mockConfig)
log('Setting globals ... DONE')
