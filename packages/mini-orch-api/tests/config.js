/* eslint-disable sort-keys */
exports.datastores = {
  documentstore: global.__DOCUMENT_STORE_URI__,
  versionstore: global.__VERSION_STORE_URI__,
  graphstore: {
    uri: 'bolt://localhost:11002/',
    username: 'stitdata',
    password: 'stit@1234',
  },
}

exports.password = {
  hashLength: 16,
  saltLength: 8,
  memoryCost: 128,
  parallelism: 1,
  timeCost: 1,
  type: 'argon2d',
}

exports.jwt = {
  publicKey: `-----BEGIN PUBLIC KEY-----
MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDpwldKsdgNCzkhqdZCDeFQrEet
mNKilb96Od7DyLtgqkdQkOjjKXzATVAG5XZ1wEoCMhoz2ePKAneIcGImDqHWgxqS
Ssncmosw+kfDyhWbVTDx9/mQ5tpeua70VVVpACtp9Kt1ZS/0Q8RgzYc7h4QrnFC0
cQYe3d+ujyaj1TV3QQIDAQAB
-----END PUBLIC KEY-----`,

  privateKey: `-----BEGIN RSA PRIVATE KEY-----
MIICXQIBAAKBgQDpwldKsdgNCzkhqdZCDeFQrEetmNKilb96Od7DyLtgqkdQkOjj
KXzATVAG5XZ1wEoCMhoz2ePKAneIcGImDqHWgxqSSsncmosw+kfDyhWbVTDx9/mQ
5tpeua70VVVpACtp9Kt1ZS/0Q8RgzYc7h4QrnFC0cQYe3d+ujyaj1TV3QQIDAQAB
AoGAccrgMYVXlinzetaxbWBSYbIsBm7RgsIA/x/yuD4cMRdTNf4E0KheiUJZhbHo
M7QSkvUO2lTkVW9T/wAuVzZiQxtI4yhDGvcJs3uMdoDjVmir/eubogazXs+Jncag
Mob3FBh0T/GyIcXES+RFCgZypcykO1CbpJG/XvPp6xnpDb0CQQD3w5mcGIMdbNKx
XioDZJJGHC/+R3Xvr7Siu490OC4JjwjOBzuo201FTwhcZ0Kx3guonzb6Y+7zMJNE
228Oz+rnAkEA8YeQLCKJ8kzt5OXULrAkbgeZ4YvM1odPJIhSr68/al93Y90jjGBv
sO9RZFnI7vfh9jJYVAA7F2uLUMU7X1KvlwJBAO+2KDV1Jc3fTs3aTA2yTS9V8D01
iG5Y8gyMKRQJkskewScMqnUJkG4hM/aq1UVfjqobOMbyzuotMh7pqz6KfTsCQQCI
PdftjW4S56IpxBr0WxexmUgMGjfpq1nRWbSKtWgoMMG/6zXqMVvPiq92086sPdeQ
cMWyXI74nwlu/lKHarTxAkArWeZ3/Hb1GDyT5cGkhF48kBvvG+FDslB16IJ0SILB
Fsh/DRq/LQTHO29+osGQzvRxjVr0JdQHyMyAwoFaU+DS
-----END RSA PRIVATE KEY-----`,
}

exports.env = process.env.NODE_ENV

exports.domain = 'mini-orch-test.stitdata.com'
/* eslint-enable sort-keys */
