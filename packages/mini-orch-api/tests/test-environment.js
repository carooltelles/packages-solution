const NodeEnvironment = require('jest-environment-node')
const log = require('debug')('test:environ')

class TestEnvironment extends NodeEnvironment {
  // eslint-disable-next-line no-useless-constructor
  constructor(config) {
    super(config)
  }

  async setup() {
    log('Setup Test Environment')

    log('Setup MongoDB Memory Server')

    const documentStoreURI = await global.__MONGOD__.getConnectionString('test')
    this.global.__DOCUMENT_STORE_URI__ = documentStoreURI
    log('global.__DOCUMENT_STORE_URI__ ', this.global.__DOCUMENT_STORE_URI__)

    const versionStoreURI = await global.__MONGOD__.getConnectionString('test-versions')
    this.global.__VERSION_STORE_URI__ = versionStoreURI
    log('global.__VERSION_STORE_URI__ ', this.global.__VERSION_STORE_URI__)

    await super.setup()
  }

  async teardown() {
    log('Teardown Test Environment')

    await super.teardown()
  }

  runScript(script) {
    return super.runScript(script)
  }
}

module.exports = TestEnvironment
