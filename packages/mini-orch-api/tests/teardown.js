const log = require('debug')('test:teardown')

const teardownMongoDB = async () => {
  log('Stop in-memory MongoDB  ...')

  await global.__MONGOD__.stop()

  delete global.__MONGOD__
  log('Stop in-memory MongoDB  ... DONE')
}

module.exports = async function() {
  log('Global teardown ...')
  await teardownMongoDB()
  log('Global teardown ... DONE')
}
