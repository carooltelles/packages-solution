const log = require('debug')('test:setup')
const MongodbMemoryServer = require('mongodb-memory-server').default

const setupMongoDB = async () => {
  log('Start in-memory MongoDB  ...')

  const mongod = new MongodbMemoryServer()

  global.__MONGOD__ = mongod

  log('Start in-memory MongoDB  ... DONE')
}

module.exports = async function() {
  log('Global setup ...')
  await setupMongoDB()
  log('Global setup ... DONE')
}
