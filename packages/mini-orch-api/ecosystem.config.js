// Options reference: https://pm2.io/doc/en/runtime/reference/ecosystem-file/
module.exports = {
  apps: [
    {
      autorestart: true,
      exec_mode: 'cluster',
      instances: 'max',
      max_memory_restart: '1G',
      name: 'mini-orch-api',
      script: 'dist/api.js',
      watch: false,
    },
    // {
    //   name: 'mini-orch-worker',
    //   script: 'dist/worker.js',
    //   instances: '2',
    //   autorestart: true,
    //   exec_mode: 'cluster',
    //   watch: false,
    //   max_memory_restart: '1G',
    // },
  ],
}
