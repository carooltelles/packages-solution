import { noop } from 'lodash'

const seed = noop

export default { seed }
