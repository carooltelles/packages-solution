/* eslint-disable sort-keys */
import { includes } from 'lodash'
import AWS from 'aws-sdk'
import os from 'os'

import dotenv from '@stit-data/dotenv'

dotenv.load({ overwrite: true })

export const domain = process.env.DOMAIN

export const server = {
  port: process.env.PORT,
  protocol: process.env.PROTOCOL,
  schema: process.env.PROTOCOL === 'http' ? 'http' : 'https',
  domain: process.env.NODE_ENV === 'production' ? domain : 'locahost', // Only for logging purposes
}

export const log = {
  level: process.env.LOGGER_LEVEL || 'debug',
  silent: includes(process.argv, '--silent'),
}

export const datastores = {
  seed: process.env.SEED || false,
  documentstore: process.env.DOCUMENT_STORE_URI,
  versionstore: process.env.VERSION_STORE_URI,
  graphstore: {
    uri: process.env.GRAPH_STORE_URI,
    username: process.env.GRAPH_STORE_USERNAME,
    password: process.env.GRAPH_STORE_PASSWORD,
  },
}

export const god = {
  user: {
    username: process.env.GOD_USER_USERNAME,
    password: process.env.GOD_USER_PASSWORD,
  },
  organization: {
    name: process.env.GOD_ORGANIZATION_NAME,
  },
}

// TODO move to .env
/* PROD should be at least and run under 500ms */
// export const password = {
//   hashLength: 1024,
//   saltLength: 256,
//   memoryCost: 2 ** 22,
//   parallelism: os.cpus().length,
//   timeCost: 8,
//   type: 'argon2id',
// }
/* good defaults */
export const password = {
  hashLength: 128,
  saltLength: 128,
  memoryCost: 102400,
  parallelism: os.cpus().length,
  timeCost: 2,
  type: 'argon2id',
}

export const jwt = {
  publicKey: process.env.JWT_PUBLIC_KEY,
  privateKey: process.env.JWT_PRIVATE_KEY,
}

export const gcloudStorage = {
  credentials: JSON.parse(process.env.GCLOUD_STORAGE_CREDENTIALS),
  mediaBucket: 'stit-mini-orch-test-bucket',
}

export const aws = {
  mediaSQS: {
    config: new AWS.Config({
      accessKeyId: process.env.AWS_SQS_MEDIA_ACCESS_ID,
      secretAccessKey: process.env.AWS_SQS_MEDIA_ACCESS_KEY,
      region: process.env.AWS_REGION,
    }),
    url: process.env.AWS_SQS_MEDIA_ACCESS_URL,
  },
}

export const env = process.env.NODE_ENV
/* eslint-enable sort-keys */
