import AWS from 'aws-sdk'
import serialize from 'serialize-javascript'

import { GCLOUD } from '../constants/service-types'
import { aws as awsConfig, gcloudStorage as gcloudStorageConfig } from '../config'
import * as gcloudStorage from '../services/gcloud-storage'
import fileRepository from '../repositories/file'
import mediaRepository from '../repositories/media'

const sqs = new AWS.SQS(awsConfig.mediaSQS.config)

const save = async ({ media, file }) => {
  try {
    const { name, type, ...mediaMetadata } = media
    const { buffer, ...fileMetadata } = file

    const url = await gcloudStorage.save({
      bucketName: gcloudStorageConfig.mediaBucket,
      fileBuffer: buffer,
      mimetype: fileMetadata.mimetype,
      originalName: fileMetadata.originalname,
    })

    const fileDocument = await fileRepository.create({
      isPublic: false,
      metadata: fileMetadata,
      service: GCLOUD,
      url,
    })
    const mediaDocument = await mediaRepository.create({
      fileObjectId: fileDocument.refId,
      metadata: mediaMetadata,
      name,
      type,
    })

    const params = {
      MessageAttributes: {
        task: {
          DataType: 'String',
          StringValue: 'extract_audio',
        },
        type: {
          DataType: 'String',
          StringValue: 'media',
        },
      },
      MessageBody: serialize({ media: mediaDocument }, { isJSON: true }),
      QueueUrl: awsConfig.mediaSQS.url,
    }

    sqs.sendMessage(params, function(err, data) {
      if (err) {
        console.log('Error', err)
      } else {
        console.log('Success', data.MessageId)
      }
    })

    return mediaDocument
    // eslint-disable-next-line sonarjs/no-useless-catch
  } catch (error) {
    // TODO: handle errors properly
    throw error
  }
}

export { save }
