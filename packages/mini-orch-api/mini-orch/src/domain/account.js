import { uuidV5 } from 'src/helpers/uniq-id'
import { issue, verify as verifyJwt } from 'src/helpers/jwt'
import { hash, verify as verifyPassword } from 'src/helpers/password'
import userRepository from 'src/repositories/user'

const register = async ({ username, password }) => {
  try {
    const uniqID = uuidV5(username, 'user')
    const hashedPassword = await hash(password)

    const newUser = await userRepository.create({
      password: hashedPassword,
      uniqID,
      username,
    })

    const user = newUser.document.toJSON()
    const token = await issue(user)

    return { token, user }
  } catch (error) {
    if (error.code === 11000 && error.name === 'MongoError') {
      throw new UsernameAlreadyTaken()
    }
    throw error
  }
}

const signIn = async ({ username, password }) => {
  const foundUser = await userRepository.findByUsername(username)
  if (!foundUser) throw new InvalidUsernamePasswordCombination()

  const passwordOk = await verifyPassword(foundUser.password, password)
  if (!passwordOk) throw new InvalidUsernamePasswordCombination()

  const user = foundUser.toJSON()
  const token = await issue(user)

  return {
    token,
    user: { ...user, password: foundUser.password },
  }
}

const authenticate = async token => {
  //  QUERY: Should I query against the database here?
  return verifyJwt(token)
}

class UsernameAlreadyTaken extends Error {
  constructor() {
    super('username_already_taken')
    Error.captureStackTrace(this, UsernameAlreadyTaken)
  }
}

class InvalidUsernamePasswordCombination extends Error {
  constructor() {
    super('invalid_username_password_combination')
    Error.captureStackTrace(this, UsernameAlreadyTaken)
  }
}

const errors = {
  InvalidUsernamePasswordCombination,
  UsernameAlreadyTaken,
}

export { register, signIn, authenticate, errors }
