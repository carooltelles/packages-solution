import { omit } from 'lodash'
import mongoose from 'mongoose'

const Schema = mongoose.Schema

const cloneSchema = schema => {
  const newSchema = new Schema()

  schema.eachPath((key, path) => {
    if (key !== '_id') {
      newSchema.add({ [key]: omit(path.options, 'unique') })
    }
  })

  return newSchema
}

const mongooseVersions = (schema, options) => {
  const versionedSchema = cloneSchema(schema)

  versionedSchema.add({
    refId: Schema.Types.ObjectId,
    refVersion: Number,
  })

  schema.statics.Version = options.store.model(options.name, versionedSchema)

  schema.pre('save', async function(next) {
    if (!this.isNew) {
      this.increment()

      const currentVersion = omit(this.toObject(), '_id')

      currentVersion.refVersion = this._doc.__v
      currentVersion.refId = this._id

      const version = new schema.statics.Version(currentVersion)

      try {
        await version.save()
        next()
      } catch (error) {
        next(error)
      }
    }
  })

  schema.pre('remove', async function(next) {
    if (!options.removeVersions) return next()

    try {
      await schema.statics.Version.deleteMany({ refId: this._id })
      return next()
    } catch (error) {
      return next(error)
    }
  })
}

export default mongooseVersions
