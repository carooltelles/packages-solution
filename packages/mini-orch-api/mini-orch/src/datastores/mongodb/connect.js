import { getLogger } from 'src/helpers/logging'
import mongoose from 'mongoose'
import mongooseUUID2 from 'mongoose-uuid2'

mongoose.Promise = global.Promise
mongooseUUID2(mongoose)

const { log, logged } = getLogger('stores:mongodb', 'connect')

const connect = logged(
  uri =>
    new Promise((resolve, reject) => {
      log.verbose(`creating connection to mongodb at: ${uri}`)
      try {
        const instance = mongoose.createConnection(uri, {
          useCreateIndex: true,
          useNewUrlParser: true,
        })
        instance.on('connected', () => {
          log.info(`connected to mongodb at: ${uri}`)
          resolve(instance)
        })
        instance.on('error', err => {
          log.error(`mongodb error at: ${uri} - ${err}`)
          reject(err)
        })
        instance.on('disconnected', () => log.info(`disconnect to mongodb at: ${uri}`))
      } catch (error) {
        log.error(`error on mongodb ${error}`)
        reject(error)
      }
    })
)

export default connect
