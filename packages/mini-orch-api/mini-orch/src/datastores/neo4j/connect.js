import { getLogger } from 'src/helpers/logging'
import neo4j from 'neo4j-driver'

const { log, logged } = getLogger('stores:neo4j')

const connect = logged('connect')(
  (uri, username, password) =>
    new Promise((resolve, reject) => {
      const credentials = neo4j.auth.basic(username, password)
      const driver = neo4j.driver(uri, credentials)

      const driverLogger = log('driver')
      driver.onCompleted = info => {
        driverLogger.info('connected', info)
        resolve(driver)
      }

      driver.onError = error => {
        driverLogger.error(error)
        reject(error)
      }
    })
)

export default connect
