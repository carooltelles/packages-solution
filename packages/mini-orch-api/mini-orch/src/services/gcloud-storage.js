import { last, split } from 'lodash'
import { Storage } from '@google-cloud/storage'
import { auth } from 'google-auth-library'

import { gcloudStorage } from '../config'
import { shortId } from '../helpers/uniq-id'

const storage = new Storage({ credentials: gcloudStorage.credentials })

const fileUrl = (fileName, bucket) => `https://storage.googleapis.com/${bucket}/${fileName}`

const generateFileName = (fileName, bucket) => `${shortId()}.${last(split(fileName, '.'))}`

// eslint-disable-next-line max-statements
const save = async ({ fileBuffer, originalName, mimetype, bucketName, makePublic }) => {
  const authClient = auth.fromJSON(gcloudStorage.credentials)

  authClient.scopes = ['https://www.googleapis.com/auth/cloud-platform']
  const url = `https://www.googleapis.com/dns/v1/projects/${gcloudStorage.credentials.project_id}`
  const res = await authClient.request({ url })
  console.log(res.data)

  console.time('gcloud-storage save')
  const bucket = storage.bucket(bucketName)

  const fileName = generateFileName(originalName)
  const file = bucket.file(fileName)

  const [resumableUri] = await file.createResumableUpload()
  console.log(resumableUri)

  const stream = file.createWriteStream({
    metadata: {
      contentType: mimetype,
    },
    uri: resumableUri,
  })
  const streamPromise = new Promise((resolve, reject) => {
    stream.on('error', reject)
    stream.on('finish', result => {
      if (makePublic) {
        file
          .makePublic()
          .then(resolve)
          .catch(reject)
      }
      resolve(result)
    })
  })

  try {
    stream.end(fileBuffer)
    await streamPromise

    console.timeEnd('gcloud-storage save')
    return fileUrl(fileName, bucketName)
    // eslint-disable-next-line sonarjs/no-useless-catch
  } catch (error) {
    // TODO: Handle errors properly
    throw error
  }
}

export { save }
