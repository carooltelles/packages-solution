const alphabets = {
  lowercase: 'abcdefghijklmnopqrstuvwxyz',
  uppercase: 'ABCDEFGHIJKLMNOPQRSTUVWXYZ',
}

const numbers = '0123456789'

const urlSafe = '_-'

const filename = urlSafe + numbers + alphabets.lowercase + alphabets.uppercase

const token = numbers + alphabets.lowercase + alphabets.uppercase

export { alphabets, numbers, filename, token }
