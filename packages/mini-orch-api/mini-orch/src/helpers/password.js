import argon2 from 'argon2'
import { password as passwordConfig } from 'src/config'
import { getLogger } from 'src/helpers/logging'

const { logged } = getLogger('password')

const hash = logged('hash')(password => argon2.hash(password, passwordConfig))

const verify = logged('verify')((hashed, received) => argon2.verify(hashed, received))

export { hash, verify }
