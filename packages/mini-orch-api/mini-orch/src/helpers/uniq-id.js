import uuidv5 from 'uuid/v5'
import * as dictionary from 'src/constants/dictionary'
import domain from 'src/helpers/domain'

const en = require('nanoid-good/locale/en')
const pt = require('nanoid-good/locale/pt')
const es = require('nanoid-good/locale/es')

var generate = require('nanoid-good/generate')(en, pt, es)

const uuidV5 = (name, scope) => uuidv5(`${name}.${domain(scope)}`, uuidv5.DNS)

const token = () => generate(dictionary.token, 6)
const shortId = () => generate(dictionary.filename, 12)
const tinyId = () => generate(dictionary.filename, 8)

export { uuidV5, shortId, tinyId, token }
