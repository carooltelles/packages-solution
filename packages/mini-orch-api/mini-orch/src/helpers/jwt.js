import jwt from 'jsonwebtoken'
import { domain, jwt as jwtConfig } from 'src/config'
import { getLogger } from 'src/helpers/logging'

const { logged } = getLogger('jwt')

const issue = logged('issue')(claims => {
  return new Promise((resolve, reject) => {
    jwt.sign(
      claims,
      jwtConfig.privateKey,
      {
        algorithm: 'RS512',
        issuer: domain,
      },
      (err, token) => {
        if (err) reject(err)
        else resolve(token)
      }
    )
  })
})

const verify = logged('verify')(token => {
  return new Promise((resolve, reject) => {
    jwt.verify(
      token,
      jwtConfig.publicKey,
      {
        algorithms: ['RS512'],
        issuer: domain,
      },
      (err, verified) => {
        if (err) {
          if (
            err instanceof jwt.JsonWebTokenError ||
            err instanceof jwt.NotBeforeError ||
            err instanceof jwt.TokenExpiredError
          ) {
            reject(new InvalidToken())
          } else {
            reject(err)
          }
        } else resolve(verified)
      }
    )
  })
})

class InvalidToken extends Error {
  constructor() {
    super('invalid_token')
    Error.captureStackTrace(this, InvalidToken)
  }
}

const errors = {
  InvalidToken,
}

export { issue, verify, errors }
