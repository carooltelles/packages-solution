import { performance } from 'perf_hooks'
import { curry, isFunction, reduce } from 'lodash'
import { format } from 'util'
import { createLogger as createWinston, transports, format as wFormat } from 'winston'
import humanizeDuration from 'humanize-duration'
import { log as logConfig } from 'src/config'

const levels = ['error', 'warn', 'info', 'verbose', 'debug', 'silly']
const winston = createWinston({
  level: logConfig.level,
  transports: [
    new transports.Console({
      colorize: true,
      format: wFormat.combine(wFormat.colorize(), wFormat.simple()),
      json: true,
      prettyPrint: true,
      silent: logConfig.silent,
      stringify: true,
    }),
  ],
})

const isPromise = obj => isFunction(obj.then)

const logPrefix = ({ namespace, label }) => `[${namespace}] ${label}`

const durationFrom = (start, humanize = true) => {
  const duration = performance.now() - start
  if (humanize) {
    return humanizeDuration(duration, { round: true, units: ['ms'] })
  }
  return duration
}

// eslint-disable-next-line lodash/prefer-lodash-method
const createLogger = curry((namespace, label) =>
  reduce(
    levels,
    (lLogger, level) => {
      lLogger[level] = (...args) =>
        winston[level](logPrefix({ label, namespace }) + format('', ...args))
      return lLogger
    },
    {}
  )
)

const done = start => `DONE in ${durationFrom(start)}`
const after = start => `after ${durationFrom(start)}`

const createLogged = curry((namespace, label) => {
  const rethrow = true
  const logger = createLogger(namespace, `${label}()`)

  return wrapped => {
    return function() {
      const start = performance.now()
      logger.verbose('START')
      try {
        const result = wrapped.apply(this, arguments)
        if (result && isPromise(result)) {
          return result
            .then(value => {
              logger.verbose(done(start), '(async)')
              return value
            })
            .catch(error => {
              logger.error(after(start), '(async)', error)
              if (rethrow) throw error
            })
        }
        logger.verbose(done(start))
        return result
      } catch (error) {
        logger.error(after(start), error)
        if (rethrow) throw error
        return null
      }
    }
  }
})

const getLogger = (...args) => ({
  log: createLogger(...args),
  logged: createLogged(...args),
})

export { getLogger }
