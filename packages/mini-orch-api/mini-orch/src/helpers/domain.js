import { domain as baseDomain } from 'src/config'

const domain = sub => `${sub}.${baseDomain}`

export default domain
