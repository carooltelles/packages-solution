import { initialize as initializeRepositories } from 'src/repositories'
import { getLogger } from 'src/helpers/logging'

const initialize = async () => {
  await initializeRepositories()
}

const { logged } = getLogger('src', 'initialize')
const loggedInitialize = logged(initialize)

export { loggedInitialize as initialize }
