import { tinyId } from 'src/helpers/uniq-id'

import documentStore from './document-store'

// file: { service, url, isPublic }
const create = async ({ name, type, metadata, fileObjectId, secondaryFileObjectId }) => {
  const MediaDocument = documentStore().model('Media')

  const mediaDoc = new MediaDocument({
    file: fileObjectId,
    metadata,
    name,
    secondaryFile: secondaryFileObjectId,
    type,
    uniqID: tinyId(),
  })

  try {
    const createdDoc = await mediaDoc.save()
    await createdDoc
      .populate('file')
      .populate('secondaryFile')
      .execPopulate()

    return createdDoc.toJSON()
    // eslint-disable-next-line sonarjs/no-useless-catch
  } catch (error) {
    // if (err.code === 11000 && err.name === 'MongoError') {
    //   // TODO: handle erros properly
    //   throw err
    // } else {
    //   throw err
    // }
    throw error
  }
}

const update = async media => {
  const MediaDocument = documentStore().model('Media')
  const { id, _id, uniqID, ...fields } = media

  try {
    const updated = await MediaDocument.findOneAndUpdate({ uniqID }, fields)
    await updated
      .populate('file')
      .populate('secondaryFile')
      .execPopulate()

    return updated.toJSON()
    // eslint-disable-next-line sonarjs/no-useless-catch
  } catch (error) {
    // TODO: handle erros properly
    throw error
  }
}

export default {
  create,
  update,
}
