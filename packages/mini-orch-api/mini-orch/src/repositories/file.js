import { shortId } from 'src/helpers/uniq-id'

import documentStore from './document-store'

const create = async ({ service, url, metadata, isPublic }) => {
  const FileDocument = documentStore().model('File')

  const fileDoc = new FileDocument({
    isPublic,
    metadata,
    service,
    uniqID: shortId(),
    url,
  })

  try {
    const createdDoc = await fileDoc.save()

    return { refId: fileDoc._id, ...createdDoc.toJSON() }
    // eslint-disable-next-line sonarjs/no-useless-catch
  } catch (error) {
    // if (err.code === 11000 && err.name === 'MongoError') {
    //   // TODO: handle erros properly
    //   throw err
    // } else {
    //   throw err
    // }
    throw error
  }
}

export default {
  create,
}
