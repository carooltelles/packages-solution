import { getLogger } from 'src/helpers/logging'

import {
  close as closeDocumentStore,
  initialize as initializeDocumentStore,
} from './document-store'
// import { close as closeGraphStore, initialize as initializeGraphStore } from './graph-store'

export { default as organization } from './organization'
export { default as user } from './user'
export { default as file } from './file'
export { default as media } from './media'

const { logged } = getLogger('repo')

const initialize = logged('initialize')(async () => {
  // await Promise.all([initializeDocumentStore(), initializeGraphStore()])
  await initializeDocumentStore()
})

const close = logged('close')(async () => {
  closeDocumentStore()
  // await Promise.all([closeDocumentStore(), closeGraphStore()])
})

export { initialize, close }
