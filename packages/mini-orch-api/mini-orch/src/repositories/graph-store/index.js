import { getLogger } from 'src/helpers/logging'
import { datastores } from 'src/config'
import { connect } from 'src/datastores/neo4j'

const { logged } = getLogger('repo:graph-store')

let graphStoreInstance

const initialize = logged('initialize')(async () => {
  const { uri, username, password } = datastores.graphstore

  graphStoreInstance = await connect(
    uri,
    username,
    password
  )
})

const graphStore = () => graphStoreInstance

const close = logged('close')(async () => {
  await graphStoreInstance.close()
})

export { graphStoreInstance, initialize, close }
export default graphStore
