import { keys, map } from 'lodash'

const formatKey = key => `${key}: {${key}}`
const toParams = obj => `{${map(keys(obj), formatKey).join(', ')}}`

export const node = ({ name, label, params }) =>
  `(${name}${label ? ':' + label : ''}${params ? ' ' + toParams(params) : ''})`

const relationship = ({ name, label, params, direction = 'right' }) => {
  const inner = `${name}${label ? ':' + label : ''}${params ? ' ' + toParams(params) : ''})`

  return {
    both: `<-[${inner}]->`,
    left: `<-[${inner}]-`,
    right: `-[${inner}]->`,
  }[direction]
}

const mergeNode = ({ label, params }) => `MERGE ${node({ label, name: 'n', params })} RETURN n;`

const mergeRelationship = (nodeN, nodeM, relationshipR) =>
  `MATCH ${node({ ...nodeN, name: 'n' })}, ${node({
    ...nodeM,
    name: 'm',
  })} MERGE ${node('n')}${relationship({ ...relationshipR, name: 'r' })}${node('m')} RETURN n,r,m;`

export { mergeNode, mergeRelationship }
