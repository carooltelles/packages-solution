// import * as cypher from './graph-store/cypher'

import documentStore from './document-store'
// import graphStore from './graph-store'

// eslint-disable-next-line max-statements
const create = async ({ uniqID, username, password }) => {
  const UserDocument = documentStore().model('User')

  let createdDocument
  try {
    const userDocument = new UserDocument({
      password,
      uniqID,
      username,
    })

    createdDocument = await userDocument.save()

    // const graphSession = graphStore().session()
    // const userNode = {
    //   label: 'User',
    //   params: { uniqID, username },
    // }

    // const txResult = await graphSession.run(cypher.mergeNode(userNode), userNode.params)
    // graphSession.close()

    // createdNode = txResult.records[0].get('n')

    // return { document: createdDocument, node: createdNode }
    return { user: createdDocument }
  } catch (error) {
    if (createdDocument) {
      await UserDocument.deleteOne({ _id: createdDocument.id })
    }
    throw error
  }
}

const findByUsername = async username => {
  const UserDocument = documentStore().model('User')
  const user = await UserDocument.findOne({ username }).populate('profile')
  return user || null
}

export default {
  create,
  findByUsername,
}
