import mongoose from 'mongoose'
import mongooseDelete from 'mongoose-delete'
import mongooseHidden from 'mongoose-hidden'
import mongooseTimestamps from 'mongoose-timestamp'

import serviceTypes from 'src/constants/service-types'

const fileSchema = (modelName, store) => {
  const Schema = mongoose.Schema

  /* eslint-disable sort-keys */
  const schema = new Schema({
    isPublic: { type: Boolean, required: true },
    metadata: { type: Object },
    service: { type: String, required: true, enum: serviceTypes },
    uniqID: { type: String, unique: true, index: true },
    url: { type: String, required: true },
  })
  /* eslint-enable sort-keys */

  schema.set('toObject', { getters: true })
  schema.set('toJSON', { getters: true })

  schema.plugin(mongooseDelete, {
    deletedAt: true,
    deletedBy: true,
    overrideMethods: true,
  })
  schema.plugin(mongooseHidden(), { hidden: { deleted: true } })
  schema.plugin(mongooseTimestamps)

  store.model(modelName, schema)
}

export default fileSchema
