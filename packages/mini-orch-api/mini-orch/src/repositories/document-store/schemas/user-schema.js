import mongoose from 'mongoose'
import mongooseDelete from 'mongoose-delete'
import mongooseHidden from 'mongoose-hidden'
import mongooseTimestamps from 'mongoose-timestamp'

const userSchema = (modelName, store) => {
  const Schema = mongoose.Schema

  /* eslint-disable sort-keys */
  const schema = new Schema({
    // email: { type: String, required: true, unique: true, index: true },
    password: { type: String, required: true },
    profile: { type: Schema.Types.ObjectId, ref: 'Profile' },
    uniqID: { type: mongoose.Types.UUID, unique: true, index: true },
    username: { type: String, required: true, unique: true, index: true },
  })
  /* eslint-enable sort-keys */

  schema.set('toObject', { getters: true })
  schema.set('toJSON', { getters: true })

  schema.plugin(mongooseDelete, {
    deletedAt: true,
    deletedBy: true,
    overrideMethods: true,
  })
  schema.plugin(mongooseHidden(), { hidden: { deleted: true, password: true } })
  schema.plugin(mongooseTimestamps)

  store.model(modelName, schema)
}

export default userSchema
