import mongoose from 'mongoose'
import mongooseAutopopulate from 'mongoose-autopopulate'
import mongooseDelete from 'mongoose-delete'
import mongooseHidden from 'mongoose-hidden'
import mongooseTimestamps from 'mongoose-timestamp'

import mongooseVersions from 'src/datastores/mongodb/plugins/mongoose-versions'

// eslint-disable-next-line max-params
const profileSchema = (modelName, store, versionStore, versionCollection) => {
  const Schema = mongoose.Schema

  /* eslint-disable sort-keys */
  const schema = new Schema({
    birthday: { type: Date },
    coverPicture: { type: Schema.Types.ObjectId, ref: 'Picture', autopoulate: true },
    lastName: { type: String },
    name: { type: String },
    picture: { type: Schema.Types.ObjectId, ref: 'Picture', autopoulate: true },
    slug: { type: String, unique: true, index: true },
    user: {
      index: true,
      ref: 'User',
      required: true,
      type: Schema.Types.ObjectId,
      unique: true,
    },
    uniqId: { type: mongoose.Types.UUID, unique: true, index: true },
  })
  /* eslint-enable sort-keys */

  schema.set('toObject', { getters: true })
  schema.set('toJSON', { getters: true })

  schema.plugin(mongooseAutopopulate)
  schema.plugin(mongooseDelete, {
    deletedAt: true,
    deletedBy: true,
    overrideMethods: true,
  })
  schema.plugin(mongooseHidden(), { hidden: { deleted: true, user: true } })
  schema.plugin(mongooseTimestamps)

  if (versionStore) {
    schema.plugin(mongooseVersions, {
      name: versionCollection || modelName,
      store: versionStore,
    })
  }

  store.model(modelName, schema)
}

export default profileSchema
