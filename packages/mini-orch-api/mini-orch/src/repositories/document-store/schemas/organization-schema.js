import mongoose from 'mongoose'
import mongooseDelete from 'mongoose-delete'
import mongooseHidden from 'mongoose-hidden'
import mongooseTimestamps from 'mongoose-timestamp'

import organizationTypes from 'src/constants/organization-types'

const organizationSchema = (modelName, store) => {
  const Schema = mongoose.Schema

  /* eslint-disable sort-keys */
  const schema = new Schema({
    name: { type: String, required: true },
    type: { type: String, required: true, enum: organizationTypes },
    uniqID: { type: mongoose.Types.UUID, unique: true, index: true },
  })
  /* eslint-enable sort-keys */

  schema.set('toObject', { getters: true })
  schema.set('toJSON', { getters: true })

  schema.plugin(mongooseDelete, {
    deletedAt: true,
    deletedBy: true,
    overrideMethods: true,
  })
  schema.plugin(mongooseHidden(), { hidden: { deleted: true } })
  schema.plugin(mongooseTimestamps)

  store.model(modelName, schema)
}

export default organizationSchema
