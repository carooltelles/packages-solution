import mongoose from 'mongoose'
import mongooseAutopopulate from 'mongoose-autopopulate'
import mongooseDelete from 'mongoose-delete'
import mongooseHidden from 'mongoose-hidden'
import mongooseTimestamps from 'mongoose-timestamp'

const pictureSchema = (modelName, store) => {
  const Schema = mongoose.Schema

  /* eslint-disable sort-keys */
  const schema = new Schema({
    alt: { type: String },
    file: { type: Schema.Types.ObjectId, ref: 'File', autopopulate: true },
    metadata: { type: Object },
    thumbnailFile: { type: Schema.Types.ObjectId, ref: 'File', autopopulate: true },
    uniqID: { type: String, unique: true, index: true },
  })
  /* eslint-enable sort-keys */

  schema.set('toObject', { getters: true })
  schema.set('toJSON', { getters: true })

  schema.plugin(mongooseAutopopulate)
  schema.plugin(mongooseDelete, {
    deletedAt: true,
    deletedBy: true,
    overrideMethods: true,
  })
  schema.plugin(mongooseHidden(), { hidden: { deleted: true } })
  schema.plugin(mongooseTimestamps)

  store.model(modelName, schema)
}

export default pictureSchema
