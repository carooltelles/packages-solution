import { getLogger } from 'src/helpers/logging'
import { datastores } from 'src/config'
import { connect } from 'src/datastores/mongodb'

import * as schemas from './schemas'

const { logged } = getLogger('repo:doc-store')

let documentStoreInstance
let versionStoreInstance

// eslint-disable-next-line max-statements
const initialize = logged('initialize')(async () => {
  const connectedStores = await Promise.all([
    connect(datastores.documentstore),
    connect(datastores.versionstore),
  ])

  documentStoreInstance = connectedStores[0]
  versionStoreInstance = connectedStores[1]

  schemas.fileSchema('File', documentStoreInstance)
  schemas.pictureSchema('Picture', documentStoreInstance)
  schemas.profileSchema('Profile', documentStoreInstance, versionStoreInstance)
  schemas.userSchema('User', documentStoreInstance)
})

const documentStore = () => documentStoreInstance

const close = logged('close')(async () => {
  await documentStoreInstance.close()
  await versionStoreInstance.close()
})

export { documentStoreInstance, initialize, close }
export default documentStore
