import { forEach, map, toLower } from 'lodash'
import Router from 'koa-router'
import { getLogger } from 'src/helpers/logging'
import getRoutes from './get-routes'

const { logged } = getLogger('www')

const createRouter = (prefix, endpoints) => {
  const router = new Router({ prefix })

  forEach(endpoints, endpoint => {
    const { method, route, handlers } = endpoint
    const verb = toLower(method)

    router[verb](route, ...handlers)
  })

  return router
}

const addRoute = async (route, mainRouter) => {
  const { endpoints } = await import(`./${route}/endpoints`)
  const prefix = route === 'home' ? '' : `/${route}`

  const router = createRouter(prefix, endpoints)
  mainRouter.use(router.routes(), router.allowedMethods())
}

const router = logged('router')(async app => {
  const mainRouter = new Router()
  const routes = await getRoutes()

  const addingRoutesPromises = map(routes, route => addRoute(route, mainRouter))

  await Promise.all(addingRoutesPromises)

  app.use(mainRouter.routes()).use(mainRouter.allowedMethods())
})

export default router
