import organizationTypes from '../../constants/organization-types'

export function validateOrganization(body) {
  body.required().isObject(entry => {
    entry('organization')
      .required()
      .isObject(child => {
        child('type')
          .required()
          .isString()
          .isIn(organizationTypes)
        child('name')
          .required()
          .isString()
      })
      .strict()
  })
}
