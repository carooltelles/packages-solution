import glob from 'glob'
import { map } from 'lodash'
import { getLogger } from 'src/helpers/logging'

import { mainRes } from '../../../scripts/config'

const basePath = `${mainRes('api', 'www')}`
const pattern = `.${basePath}/**/endpoints.js`
const { log, logged } = getLogger('www', 'getRoutes')

const getRoutes = async () => {
  log.verbose('searching pattern', pattern)
  const globPromise = new Promise((resolve, reject) => {
    glob(pattern, null, (err, matches) => {
      if (err) return reject(err)
      // eslint-disable-next-line security/detect-non-literal-regexp
      const regex = new RegExp(`(${basePath}/)(.*)(/endpoints.js)`)
      return resolve(map(matches, m => regex.exec(m)[2]))
    })
  })

  const matches = await globPromise

  log.info('found', matches)
  return matches
}

export default logged(getRoutes)
