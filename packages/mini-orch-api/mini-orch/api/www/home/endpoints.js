import { authorize } from '../../middlewares/authentication'

const endpoints = [
  {
    handlers: [
      ctx => {
        ctx.body = 'STIT MiniOrch'
      },
    ],
    method: 'GET',
    route: '/',
  },
  {
    handlers: [
      authorize(),
      ctx => {
        ctx.body = `You are authorized with\n${JSON.stringify(ctx.state.user, null, 2)}`
      },
    ],
    method: 'GET',
    route: '/secret',
  },
]

export { endpoints }
