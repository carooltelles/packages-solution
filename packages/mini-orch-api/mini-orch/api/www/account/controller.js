import * as account from 'src/domain/account'

const register = async (ctx, next) => {
  try {
    // Since the body is already validated, I think it safe to use body.user directly
    const { user, token } = await account.register(ctx.request.body.user)

    ctx.body = {
      token,
      user,
    }
  } catch (error) {
    if (error instanceof account.errors.UsernameAlreadyTaken) {
      ctx.throw(409, error.message)
    } else {
      ctx.throw(500, error.message)
    }
  }

  if (next) {
    return next()
  }
  return null
}

const signIn = async (ctx, next) => {
  try {
    const { password, username } = ctx.request.body.user

    const { user, token } = await account.signIn({ password, username })

    ctx.body = {
      token,
      user,
    }
  } catch (error) {
    if (error instanceof account.errors.InvalidUsernamePasswordCombination) {
      ctx.throw(401, error.message)
    } else {
      ctx.throw(500)
    }
  }

  if (next) {
    return next()
  }
  return null
}

export { register, signIn }
