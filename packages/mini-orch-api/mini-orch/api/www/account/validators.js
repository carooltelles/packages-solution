export function validateAccount(body) {
  body.required().isObject(entry => {
    entry('user')
      .required()
      .isObject(child => {
        child('username')
          .required()
          .isString()
        child('password')
          .required()
          .isString()
      })
      .strict()
  })
}
