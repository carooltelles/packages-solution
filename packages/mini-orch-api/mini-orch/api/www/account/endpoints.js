import { validateRequestBody } from '../../middlewares/validate'
import { validateAccount } from './validators'
import * as account from './controller'

const endpoints = [
  {
    handlers: [validateRequestBody(validateAccount), account.register],
    method: 'POST',
    route: '/register',
  },
  {
    handlers: [validateRequestBody(validateAccount), account.signIn],
    method: 'POST',
    route: '/signin',
  },
]

export { endpoints }
