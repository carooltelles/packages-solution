import bodyParser from 'koa-bodyparser'
import compress from 'koa-compress'
import helmet from 'koa-helmet'
import Koa from 'koa'
import logger from 'koa-logger'
import { getLogger } from 'src/helpers/logging'

import router from './www/router'
import authentication from './middlewares/authentication'

const { log } = getLogger('www', 'handler')

const create = async () => {
  const app = new Koa()

  app.use(bodyParser())

  // TODO: Write own logger
  app.use(
    logger({
      transporter: (str, args) => {
        log.info(str)
      },
    })
  )

  authentication(app)
  await router(app)

  app.use(helmet())
  app.use(compress())

  return app
}

export { create }
