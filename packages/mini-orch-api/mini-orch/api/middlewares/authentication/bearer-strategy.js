import { Strategy as BearerStrategy } from 'passport-http-bearer'
import passport from 'koa-passport'
import { authenticate } from 'src/domain/account'

passport.use(
  'bearer',
  new BearerStrategy(async (token, done) => {
    try {
      const user = await authenticate(token)

      return done(null, user)
    } catch (error) {
      if (error) {
        return done(error)
      }
      return null
    }
  })
)
