import passport from 'koa-passport'

const authenticate = () => passport.authenticate('bearer', { session: false })

export default authenticate
