import { concat, forEach, get, has, intersection, noop, split } from 'lodash'
import { flatten } from 'flat'
import { errors as jwtErrors } from 'src/helpers/jwt'

import authenticate from './authenticate'

const authenticateMiddleware = authenticate()

// eslint-disable-next-line max-statements
const authorize = verify => async (ctx, next) => {
  delete ctx.state.user
  try {
    await authenticateMiddleware(ctx, noop)
  } catch (error) {
    if (error instanceof jwtErrors.InvalidToken) {
      ctx.throw(401, JSON.stringify({ failed: 'invalid token' }, null, 2))
    }
    throw error
  }

  const { user } = ctx.state

  if (!user) {
    ctx.throw(401, JSON.stringify({ failed: 'unauthenticated' }, null, 2))
  }

  const errors = []

  if (verify) {
    forEach(flatten(verify, { safe: true }), (val, p) => {
      if (!has(user, p)) {
        errors.push({ failed: 'required', path: split(p, '.') })
      } else {
        const userProp = concat([], get(user, p))
        const arr = concat([], val)

        if (intersection(arr, userProp).length === 0) {
          errors.push({
            expected: arr,
            failed: 'unauthorized',
            found: userProp,
            path: split(p, '.'),
          })
        }
      }
    })
  }

  if (errors.length > 0) ctx.throw(401, JSON.stringify(errors, null, 2))

  if (next) {
    return next()
  }
  return null
}

export default authorize
