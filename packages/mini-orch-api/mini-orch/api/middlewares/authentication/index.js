import passport from 'koa-passport'

import './bearer-strategy'

const authentication = app => {
  app.use(passport.initialize())
}

export default authentication

// export { default as authenticate } from './authenticate'

export { default as authorize } from './authorize'
