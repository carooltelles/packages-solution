import { get } from 'lodash'
import Validator from 'better-validator'

function validate(check, part) {
  return function(ctx, next) {
    const validator = new Validator()
    const errors = validator(get(ctx, part), check)

    if (errors && errors.length > 0) {
      ctx.throw(400, JSON.stringify(errors, null, 2))
    }

    if (next) {
      return next()
    }
    return null
  }
}

export function validateQuery(check) {
  return validate(check, 'query')
}

export function validateRequestBody(check) {
  return validate(check, 'request.body')
}

export function validateBody(check) {
  return validate(check, 'body')
}
