import ExpressMulter from 'multer'
import { forEach, noop } from 'lodash'

const multer = options => {
  const expressMulter = ExpressMulter(options)

  forEach(['any', 'array', 'fields', 'none', 'single'], prop => {
    expressMulter[prop] = convertToKoaMiddleware(expressMulter[prop])
  })

  return expressMulter
}

// QUERY: Move to helpers?
const convertToKoaMiddleware = expressMiddleware => {
  if (!expressMiddleware) return noop

  return function() {
    const middleware = expressMiddleware.apply(this, arguments)

    return (ctx, next) => {
      return new Promise((resolve, reject) => {
        middleware(ctx.req, ctx.res, err => {
          err ? reject(err) : resolve(ctx)
        })
      }).then(next)
    }
  }
}

const DiskStorage = ExpressMulter.diskStorage
const MemoryStorage = ExpressMulter.memoryStorage

export default multer
export { DiskStorage, MemoryStorage }
