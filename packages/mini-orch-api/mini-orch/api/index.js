import { initialize } from 'src'
import { server as serverConfig } from 'src/config'
import { getLogger } from 'src/helpers/logging'

import { create as createHandler } from './handler'
import { create as createServer } from './server'

const { log, logged } = getLogger('api')

const api = async () => {
  await initialize()
  const handler = await createHandler()

  const server = createServer(handler.callback(), serverConfig.protocol)

  server.listen(serverConfig.port, error => {
    const serverLog = log('server')
    if (error) {
      serverLog.error(error)
      // eslint-disable-next-line unicorn/no-process-exit
      process.exit(1)
    } else {
      serverLog.info(
        `serving at ${serverConfig.schema}://${serverConfig.domain}:${serverConfig.port}`
      )
    }
  })
}

logged('main')(api)()
