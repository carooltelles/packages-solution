import { resolve } from 'path'
import fs from 'fs'

import { getLogger } from 'src/helpers/logging'

const { log, logged } = getLogger('api', 'create server')

const getCredentials = () => ({
  cert: fs.readFileSync(resolve('.secrets/localhost.cert')),
  key: fs.readFileSync(resolve('.secrets/localhost.key')),
})

const createServer = (protocol, app) => {
  switch (protocol) {
    case 'http':
      const httpServer = require('http')
      return httpServer.createServer(app)
    case 'https':
      const httpsServer = require('https')
      return httpsServer.createServer(app, getCredentials())
    case 'http2':
      const http2Server = require('http2')
      return http2Server.createServer(app, getCredentials())
    default:
      return null
  }
}

const create = logged((app, protocol) => {
  const server = createServer(protocol, app)

  server.on('error', err => log.error(err))
  server.on('socketError', err => log.error(err))

  return server
})

export { create }
