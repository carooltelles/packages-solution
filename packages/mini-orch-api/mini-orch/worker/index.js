import AWS from 'aws-sdk'
import sqsConsumer from '@stit-data/sqs-consumer'

// import { initialize } from 'src'
import { aws as awsConfig } from 'src/config'
import { getLogger } from 'src/helpers/logging'

const { log, logged } = getLogger('worker')

// const worker = async () => {
//   await initialize()
//
const worker = () => {
  const sqsLogger = log('media consumer')

  const mediaSQSConsumer = sqsConsumer({
    handleMessage: (message, done) => {
      setTimeout(() => {
        sqsLogger.debug(message)
        done()
      })
    },
    queueUrl: awsConfig.mediaSQS.url,
    sqs: new AWS.SQS(awsConfig.mediaSQS.config),
  })

  mediaSQSConsumer.on('error', err => sqsLogger.error('error', err))
  mediaSQSConsumer.on('polling', () => sqsLogger.verbose('polling'))

  mediaSQSConsumer.start()
}

logged('main')(worker)()
