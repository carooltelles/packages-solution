/* eslint-disable sort-keys */
const path = require('path')
const RunNodeWebpackPlugin = require('run-node-webpack-plugin')
const webpack = require('webpack')
const { map } = require('lodash')

const { build } = require('../scripts/config')
const scriptsToRun = map(
  ['api', 'worker'],
  script =>
    new RunNodeWebpackPlugin({
      scriptToRun: path.join(build.output, `${script}.js`),
    })
)

module.exports = {
  target: 'node',
  mode: 'development',
  devtool: 'inline-source-map',
  entry: build.entry,
  externals: build.externals(),
  output: {
    filename: '[name].js',
    chunkFilename: '[name].js',
    libraryTarget: 'commonjs2',
    path: build.output,
    devtoolModuleFilenameTemplate: '[absolute-resource-path]',
  },
  module: {
    rules: [
      {
        test: /\.jsx?$/,
        exclude: /node_modules/,
        use: [
          {
            loader: 'babel-loader',
            options: {
              configFile: build.babel,
            },
          },
          { loader: 'eslint-loader' },
        ],
      },
    ],
  },
  resolve: build.resolve,
  plugins: [
    new webpack.DefinePlugin(build.globals),
    new webpack.NormalModuleReplacementPlugin(/^any-promise$/, 'bluebird'),
    new webpack.NormalModuleReplacementPlugin(/^node-uuid$/, 'uuid'),
    new webpack.NamedModulesPlugin(),
    ...scriptsToRun,
  ],
}
/* eslint-enable sort-keys */
