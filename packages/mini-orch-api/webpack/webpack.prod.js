/* eslint-disable sort-keys */
const Visualizer = require('webpack-visualizer-plugin')
const webpack = require('webpack')

const { build } = require('../scripts/config')

module.exports = {
  target: 'node',
  mode: 'production',
  entry: build.entry,
  externals: build.externals(),
  output: {
    filename: '[name].js',
    chunkFilename: '[name].js',
    libraryTarget: 'commonjs2',
    path: build.output,
  },
  module: {
    rules: [
      {
        test: /\.jsx?$/,
        exclude: /node_modules/,
        use: [
          {
            loader: 'babel-loader',
            options: {
              configFile: build.babel,
            },
          },
        ],
      },
    ],
  },
  resolve: build.resolve,
  plugins: [
    new webpack.DefinePlugin(build.globals),
    new webpack.NormalModuleReplacementPlugin(/^any-promise$/, 'bluebird'),
    new webpack.NormalModuleReplacementPlugin(/^node-uuid$/, 'uuid'),
    new webpack.HashedModuleIdsPlugin(),
    new Visualizer(),
  ],
}
/* eslint-enable sort-keys */
