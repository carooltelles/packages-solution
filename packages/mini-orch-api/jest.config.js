const isWatching = process.env.TEST_ENV === 'watch'

module.exports = {
  collectCoverageFrom: ['mini-orch/**/*.{js,jsx}', '!**/index.{js,jsx}'],
  coverageDirectory: 'logs/jest/coverage',
  coverageReporters: isWatching ? ['lcov'] : ['text', 'lcov'],
  globalSetup: './tests/setup.js',
  globalTeardown: './tests/teardown.js',
  globals: {
    __DEV__: true,
  },
  moduleNameMapper: {
    '^src(.*)$': '<rootDir>/mini-orch/src$1',
  },
  setupTestFrameworkScriptFile: './tests/globals.js',
  testEnvironment: './tests/test-environment.js',
  transform: {
    '^.+\\.jsx?$': 'babel-jest',
  },
}
