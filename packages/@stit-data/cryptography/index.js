import crypto from 'crypto'

const aes256gcm = (key, deriveIterations = 65536) => {
  const ALGORITHM = 'aes-256-gcm'
  const ENCODING = 'utf8'
  const BASE = 'base64'
  const HASH_ALGORITHM = 'sha512'
  const SALT_SIZE = 128

  const encrypt = str => {
    const iv = Buffer.from(crypto.randomBytes(16), ENCODING)
    const salt = crypto.randomBytes(SALT_SIZE)

    const derivedKey = crypto.pbkdf2Sync(key, salt, deriveIterations, 32, HASH_ALGORITHM)
    const cipher = crypto.createCipheriv(ALGORITHM, derivedKey, iv)
    const encrypted = Buffer.concat([cipher.update(str, ENCODING), cipher.final()])
    const tag = cipher.getAuthTag()

    return Buffer.concat([salt, iv, tag, encrypted]).toString(BASE)
  }

  const decrypt = encrypted => {
    const dataBuffer = Buffer.from(encrypted, BASE)
    const salt = dataBuffer.slice(0, SALT_SIZE)
    const iv = dataBuffer.slice(SALT_SIZE, SALT_SIZE + 16)
    const tag = dataBuffer.slice(SALT_SIZE + 16, SALT_SIZE + 32)
    const text = dataBuffer.slice(SALT_SIZE + 32)

    const derivedKey = crypto.pbkdf2Sync(key, salt, deriveIterations, 32, HASH_ALGORITHM)

    const decipher = crypto.createDecipheriv(ALGORITHM, derivedKey, iv)
    decipher.setAuthTag(tag)

    return decipher.update(text, BASE, ENCODING) + decipher.final(ENCODING)
  }

  return {
    decrypt,
    encrypt,
  }
}

const randomKey = () => Buffer.from(crypto.randomBytes(64), 'utf8')

export { aes256gcm, randomKey }
