import { each, series } from 'async'
import { forEach, forOwn, noop } from 'lodash'

const defaults = {
  attributeNames: ['SentTimestamp'],
  batchSize: 1,
  maxNumberOfMessages: 1,
  messageAttributeNames: ['All'],
  visibilityTimeout: 20,
  waitTimeSeconds: 20,
}

// eslint-disable-next-line max-statements
const sqsConsumer = options => {
  let stopped = true
  const config = { ...defaults, ...options }
  const listeners = new Map()
  const sqs = config.sqs

  const on = (label, cb) => {
    listeners.has(label) || listeners.set(label, [])
    listeners.get(label).push(cb)
  }

  const start = () => {
    if (stopped) {
      stopped = false
      poll()
    }
  }

  const stop = () => {
    stopped = true
  }

  const emit = (label, ...args) => {
    let eventListeners = listeners.get(label)

    // eslint-disable-next-line unicorn/explicit-length-check
    if (eventListeners && eventListeners.length) {
      forEach(eventListeners, listener => {
        listener(...args)
      })
    }
  }

  const poll = () => {
    const receiveParams = {
      AttributeNames: config.attributeNames,
      MaxNumberOfMessages: config.batchSize,
      MessageAttributeNames: config.messageAttributeNames,
      QueueUrl: config.queueUrl,
      VisibilityTimeout: config.visibilityTimeout,
      WaitTimeSeconds: config.waitTimeSeconds,
    }

    if (!stopped) {
      emit('polling', receiveParams)
      sqs.receiveMessage(receiveParams, handleResponse)
    } else {
      emit('stopped')
    }
  }

  const handleResponse = (err, response) => {
    if (err) {
      emit('error', err)
    }

    if (response && response.Messages && response.Messages.length > 0) {
      each(response.Messages, processMessage, () => {
        emit('response_processed')
        poll()
      })
    } else {
      if (response && !response.Messages) emit('empty')
      poll()
    }
  }

  const handleMessage = config.handleMessage || noop

  const processMessage = (message, cb) => {
    emit('message_received')

    series(
      [
        done => {
          try {
            // TODO: write this properly, immutable
            forOwn(message.MessageAttributes, (values, attribute) => {
              message.MessageAttributes[attribute] =
                values[`${values.DataType === 'Number' ? 'String' : values.DataType}Value`]
            })

            handleMessage(message, done)
          } catch (error) {
            emit('error', error)
            done(error)
          }
        },
        done => deleteMessage(message, done),
      ],
      err => {
        if (err) emit('error', err)
        cb()
      }
    )
  }

  const deleteMessage = (message, cb) => {
    const deleteParams = {
      QueueUrl: config.queueUrl,
      ReceiptHandle: message.ReceiptHandle,
    }
    sqs.deleteMessage(deleteParams, err => {
      if (err) {
        emit('error', err)
        cb(err)
      } else {
        emit('message_deleted', message)
        cb()
      }
    })
  }

  return {
    on,
    start,
    stop,
  }
}

export default sqsConsumer
