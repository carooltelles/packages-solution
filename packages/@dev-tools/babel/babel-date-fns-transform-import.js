const { snakeCase } = require('lodash')

module.exports = importName => `date-fns/${snakeCase(importName)}/index.js`
