module.exports = function() {
  return {
    plugins: [
      [require('@babel/plugin-proposal-class-properties'), { loose: false }],
      [require('@babel/plugin-proposal-decorators'), { legacy: true }],
      require('@babel/plugin-proposal-export-namespace-from'),
      require('@babel/plugin-proposal-function-sent'),
      require('@babel/plugin-proposal-json-strings'),
      require('@babel/plugin-proposal-numeric-separator'),
      require('@babel/plugin-proposal-optional-chaining'),
      require('@babel/plugin-proposal-throw-expressions'),
      require('@babel/plugin-syntax-dynamic-import'),
      require('@babel/plugin-syntax-import-meta'),
    ],
  }
}
